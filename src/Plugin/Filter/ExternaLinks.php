<?php

declare(strict_types=1);

namespace Drupal\dsfr4drupal_links\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manage DSFR external links behaviors.
 *
 * @Filter(
 *   id = "dsfr4drupal_external_links",
 *   title = @Translation("DSFR external links."),
 *   description = @Translation("Manage DSFR external links behaviors."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
final class ExternaLinks extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The DSFR Links settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->config = $container->get('config.factory')
      ->get('dsfr4drupal_links.settings');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $result = new FilterProcessResult($text);

    // If we don't want to force to open in new window.
    if (!$this->config->get('external_blank')) {
      return $result;
    }

    $dom = Html::load($text);
    $links = $dom->getElementsByTagName('a');

    foreach ($links as $link) {
      $href = $link->getAttribute('href');
      if (!$href) {
        continue;
      }

      // Check if it's a mailto link.
      if (str_starts_with($href, 'mailto:')) {
        continue;
      }

      // This is external links.
      if (
        $this->isExternalUrl($href) &&
        $link->getAttribute('target') !== '_blank'
      ) {
        // Open a link in a new tab.
        $link->setAttribute('target', '_blank');
      }

      // When link is open in a new window only.
      if ($link->getAttribute('target') !== '_blank') {
        continue;
      }

      // Add rel attribute for security and SEO.
      $link->setAttribute(
        'rel',
        implode(' ', array_unique(array_merge(
           explode(' ', $link->getAttribute('rel')),
          ['noopener', 'external']
        )))
      );

      // Add "new window" mention in title attribute.
      $title = $link->getAttribute('title');
      if (empty($title)) {
        $title = strip_tags($link->nodeValue);
      }
      $link->setAttribute('title',
        $this->t(
          '@label - new window',
          ['@label' => $title],
          ['context' => 'Link title attribute']
        )->render()
      );
    }

    return $result->setProcessedText(Html::serialize($dom));
  }

  /**
   * Check external links.
   *
   * @param string $url
   *   The given URL.
   *
   * @return bool
   *   Check url is external or not.
   */
  private function isExternalUrl(string $url): bool {
    global $base_url;

    return (
      !preg_match('#^(' . preg_quote($base_url, '#') . ')#', $url) &&
      UrlHelper::isExternal($url)
    );
  }

}
