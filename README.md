# DSFR for Drupal - Links

## Introduction

Module relating to "Design Système de l'État" ([DSFR (in French)](https://www.systeme-de-design.gouv.fr/)) recommendations.

Allows the automatic addition of a `target="_blank"` on all links considered external based on Drupal link generation.
The addition of this attribute is therefore carried out in the backend and not on the client side (in Javascript).

It also adds a `rel="noopener external"` attribute on external links, as recommended by the "Design Système de l'État" ([DSFR (in French)](https://www.systeme-de-design.gouv.fr/)).

It also allows the management of the target attribute on links in CKEditor with the provision of a dedicated filter.

## Useful links

* [Link component documentation (in French)](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/lien)
